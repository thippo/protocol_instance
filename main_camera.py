import csv
import datetime
import cv2
from flask import Flask, request, session, g, make_response, flash, render_template, redirect, url_for, jsonify, Response
import sqlite3
import time

users = ["chenjh", "mahf", "hujl"]

class schedule_obj():

    def __init__(self, filename='./static/schedule/schedule.csv'):
        with open(filename) as F:
            self.schedule_list = list(csv.reader(F))
        self.days_sorted_index = self._get_timestamps()

    def _get_timestamps(self):
        timestamp_list = []
        for i in self.schedule_list:
            timestamp_list.append(datetime.datetime.strptime(i[1], "%Y-%m-%d").timestamp())
        days_sorted_index = sorted(range(len(timestamp_list)), key=lambda k: timestamp_list[k])
        return days_sorted_index

    def _diff_day(self, date):
        return (datetime.datetime.strptime(date, "%Y-%m-%d") - datetime.datetime.now()).days

    def get_web_data(self):
        retrun_data = []
        last_list = []
        for i in self.days_sorted_index:
            if float(self.schedule_list[i][2]) == 1:
                last_list.append([self.schedule_list[i][0], self.schedule_list[i][1], 'success', str(float(self.schedule_list[i][2])*100)])
            elif self._diff_day(self.schedule_list[i][1]) >= 0 and self._diff_day(self.schedule_list[i][1]) <= 15:
                retrun_data.append([self.schedule_list[i][0], self.schedule_list[i][1], 'warning', str(float(self.schedule_list[i][2])*100)])
            elif self._diff_day(self.schedule_list[i][1]) < 0:
                retrun_data.append([self.schedule_list[i][0], self.schedule_list[i][1], 'danger', str(float(self.schedule_list[i][2])*100)])
            else:
                retrun_data.append([self.schedule_list[i][0], self.schedule_list[i][1], '', str(float(self.schedule_list[i][2])*100)])
        retrun_data = retrun_data + last_list
        return retrun_data


class VideoCamera(object):
    def __init__(self, num):
        # Using OpenCV to capture from device 0. If you have trouble capturing
        # from a webcam, comment the line below out and use a video file
        # instead.
        self.video = cv2.VideoCapture(num)
        print(num)
        # If you decide to use video.mp4, you must have this file in the folder
        # as the main.py.
        # self.video = cv2.VideoCapture('video.mp4')
    
    def __del__(self):
        self.video.release()
    
    def get_frame(self, pos):
        n, t = pos.split("_")
        success, image = self.video.read()
        shape = image.shape
        if int(t) == 1:
            image = image
        elif int(t) == 2:
            wc = shape[1]//2
            hc = shape[0]
            if int(n) == 1:
                image = image[:, :wc]
            elif int(n) == 2:
                image = image[:, wc:]
            else:
                pass
        elif int(t) == 4:
            wc = shape[1]//2
            hc = shape[0]//2
            if int(n) == 1:
                image = image[:hc, :wc]
            elif int(n) == 2:
                image = image[:hc, wc:]
            elif int(n) == 3:
                image = image[hc:, :wc]
            elif int(n) == 4:
                image = image[hc:, wc:]
            else:
                pass
        elif int(t) == 9:
            wc = shape[1]//3
            hc = shape[0]//3
            if int(n) == 1:
                image = image[:hc, :wc]
            elif int(n) == 2:
                image = image[:hc, wc:wc*2]
            elif int(n) == 3:
                image = image[:hc, wc*2:]
            elif int(n) == 4:
                image = image[hc:hc*2, :wc]
            elif int(n) == 5:
                image = image[hc:hc*2, wc:wc*2]
            elif int(n) == 6:
                image = image[hc:hc*2, wc*2:]
            elif int(n) == 7:
                image = image[hc*2:, :wc]
            elif int(n) == 8:
                image = image[hc*2:, wc:wc*2]
            elif int(n) == 9:
                image = image[hc*2:, wc*2:]
            else:
                pass
        else:
            pass
        #image = cv2.flip(image, 1)
        # We are using Motion JPEG, but OpenCV defaults to capture raw images,
        # so we must encode it into JPEG in order to correctly display the
        # video stream.
        ret, jpeg = cv2.imencode('.jpg', image)
        return jpeg.tobytes()


app = Flask(__name__)

@app.route('/schedule/index', methods=['GET', 'POST'])
def schedule_index():
    sobj = schedule_obj()
    with open('static/schedule/attention.th') as F:
        attention = F.read()
    return render_template('schedule/index', info=sobj.get_web_data(), attention=attention)

@app.route('/schedule/mgt', methods=['GET', 'POST'])
def schedule_mgt():
    with open('static/schedule/schedule.csv') as F:
        content = F.read()
    with open('static/schedule/attention.th') as F:
        attention = F.read()
    return render_template('schedule/mgt', content=content, attention=attention)

@app.route('/schedule/getcontent', methods=['GET', 'POST'])
def schedule_getcontent():
    if request.method == 'POST':
        with open('static/schedule/schedule.csv','w') as F:
            for i in request.form.get('content', '').split('\n'):
                if i:
                    F.write(i+'\n')
        with open('static/schedule/attention.th','w') as F:
            for i in request.form.get('attention', ''):
                if i:
                    F.write(i+'\n')
        return jsonify({'st':1})

@app.route('/schedule/maintain', methods=['GET', 'POST'])
def schedule_maintain():
    maintain_dict = {}
    with open('static/schedule/maintain.th') as F:
        for i in F.readlines():
            i = i.rstrip()
            if '#' in i and i:
                lin = i.replace('#', '')
                maintain_dict[lin] = []
            elif '#' not in i and i:
                maintain_dict[lin].append(i)
    return render_template('schedule/maintain', info=maintain_dict)

@app.route('/schedule/maintainmgt', methods=['GET', 'POST'])
def schedule_maintainmgt():
    with open('static/schedule/maintain.th') as F:
        maintain = F.read()
    return render_template('schedule/maintainmgt', content=maintain)

@app.route('/schedule/getmaintain', methods=['GET', 'POST'])
def schedule_getmaintain():
    if request.method == 'POST':
        with open('static/schedule/maintain.th','w') as F:
            for i in request.form.get('content', '').split('\n'):
                if i:
                    F.write(i+'\n')
        return jsonify({'st':1})


@app.route('/choosedir/index')
def choosedir_index():
    conn = sqlite3.connect("unselect.sql")
    cur = conn.cursor()
    unselect_table = request.form.get('shareem', '') + "_" + request.form.get('titan', '')
    unselect_num = cur.execute("SELECT Count(*) FROM ShareEM2020_TitanD3172").fetchall()[0][0]
    unselect_num += cur.execute("SELECT Count(*) FROM ShareEM2020_TitanD3418").fetchall()[0][0]
    unselect_num += cur.execute("SELECT Count(*) FROM ShareEM2020_TitanD3424").fetchall()[0][0]
    unselect_num += cur.execute("SELECT Count(*) FROM ShareEM2020_TitanD3786").fetchall()[0][0]
    unselect_num += cur.execute("SELECT Count(*) FROM ShareEM2022_TitanD3172").fetchall()[0][0]
    unselect_num += cur.execute("SELECT Count(*) FROM ShareEM2022_TitanD3418").fetchall()[0][0]
    unselect_num += cur.execute("SELECT Count(*) FROM ShareEM2022_TitanD3424").fetchall()[0][0]
    unselect_num += cur.execute("SELECT Count(*) FROM ShareEM2022_TitanD3786").fetchall()[0][0]
    cur.close()
    conn.close()
    conn = sqlite3.connect("selected.sql")
    cur = conn.cursor()
    selected_num = []
    for i in users:
        selected_num.append(cur.execute("SELECT Count(*) FROM "+i).fetchall()[0][0])
    cur.close()
    conn.close()
    conn = sqlite3.connect("done.sql")
    cur = conn.cursor()
    done_num = []
    for i in users:
        done_num.append(cur.execute("SELECT Count(*) FROM dones WHERE user='"+i+"'").fetchall()[0][0])
    last_20 = cur.execute("SELECT * FROM dones  order by ts desc limit 0,20").fetchall()
    last_20 = [[time.strftime("%b %d %H:%M:%S %Y", time.localtime(x[0])), x[1], x[2], x[3]] for x in last_20]
    cur.close()
    conn.close()
    conn = sqlite3.connect("selected.sql")
    cur = conn.cursor()
    selected_data_user = []
    for i in users:
        cur.execute("SELECT * FROM "+i)
        selected_data_user.append([x[0] for x in cur.fetchall()])
    cur.close()
    conn.close()
    return render_template('choosedir/index', unselect_num=unselect_num, selected_num=selected_num, done_num=done_num, last_20=last_20, selected_data_user=selected_data_user)

@app.route('/choosedir/work/<user>')
def choosedir_work(user):
    return render_template('choosedir/work', user=user)

@app.route('/choosedir/insert')
def choosedir_insert():
    return render_template('choosedir/insert')

@app.route('/choosedir/ajaxupdateall', methods=['POST'])
def choosedir_ajaxupdateall():
    conn = sqlite3.connect("unselect.sql")
    cur = conn.cursor()
    unselect_table = request.form.get('shareem', '') + "_" + request.form.get('titan', '')
    cur.execute("SELECT * FROM "+unselect_table)
    unselect_data = [x[0] for x in cur.fetchall()]
    cur.close()
    conn.close()
    conn = sqlite3.connect("selected.sql")
    cur = conn.cursor()
    selected_table = request.form.get('user', '')
    cur.execute("SELECT * FROM "+selected_table)
    selected_data = [x[0] for x in cur.fetchall()]
    cur.close()
    conn.close()
    conn = sqlite3.connect("done.sql")
    cur = conn.cursor()
    cur.execute("SELECT * FROM dones WHERE user='"+request.form.get('user', '')+"'")
    done_data = [str(x[0])+"-->"+x[1]+"-->"+x[2]+"-->"+x[3] for x in cur.fetchall()]
    cur.close()
    conn.close()
    if request.method == 'POST':
        return jsonify({'unselect_data':unselect_data, 'selected_data':selected_data, 'done_data':done_data})

@app.route('/choosedir/ajaxintoselected', methods=['POST'])
def choosedir_ajaxintoselected():
    conn = sqlite3.connect("selected.sql")
    cur = conn.cursor()
    selected_table = request.form.get('user', '')
    selected_path = "/"+request.form.get('shareem', '')+"/"+request.form.get('titan', '')+"/"+request.form.get('unselect_dir', '')
    cur.execute("INSERT INTO "+selected_table+" VALUES ('"+selected_path+"')")
    conn.commit()
    cur.close()
    conn.close()
    conn = sqlite3.connect("unselect.sql")
    cur = conn.cursor()
    unselect_table = request.form.get('shareem', '') + "_" + request.form.get('titan', '')
    cur.execute("DELETE FROM "+unselect_table+" WHERE dir="+"'"+request.form.get('unselect_dir', '')+"'")
    conn.commit()
    cur.close()
    conn.close()
    if request.method == 'POST':
        return jsonify({'status':1})

@app.route('/choosedir/ajaxbackunselect', methods=['POST'])
def choosedir_ajaxbackunselect():
    conn = sqlite3.connect("unselect.sql")
    cur = conn.cursor()
    selected_dir_split = request.form.get('selected_dir', '').split("/")
    unselect_table = selected_dir_split[1]+"_"+selected_dir_split[2]
    cur.execute("INSERT INTO "+unselect_table+" VALUES ('"+selected_dir_split[3]+"')")
    conn.commit()
    cur.close()
    conn.close()
    conn = sqlite3.connect("selected.sql")
    cur = conn.cursor()
    selected_table = request.form.get('user', '')
    cur.execute("DELETE FROM "+selected_table+" WHERE dir='"+request.form.get('selected_dir', '')+"'")
    conn.commit()
    cur.close()
    conn.close()
    if request.method == 'POST':
        return jsonify({'status':1})

@app.route('/choosedir/ajaxintodone', methods=['POST'])
def choosedir_ajaxintodone():
    ts = str(time.time())
    conn = sqlite3.connect("done.sql")
    cur = conn.cursor()
    selected_dir_split = request.form.get('selected_dir', '').split("/")
    done_path = "/"+request.form.get('storage', '')+"/"+selected_dir_split[3]
    cur.execute("INSERT INTO dones VALUES ("+ts+", '"+request.form.get('user', '')+"', '"+request.form.get('selected_dir', '')+"', '"+done_path+"')")
    conn.commit()
    cur.close()
    conn.close()
    conn = sqlite3.connect("selected.sql")
    cur = conn.cursor()
    selected_table = request.form.get('user', '')
    cur.execute("DELETE FROM "+selected_table+" WHERE dir="+"'"+request.form.get('selected_dir', '')+"'")
    conn.commit()
    cur.close()
    conn.close()
    if request.method == 'POST':
        return jsonify({'status':1})

@app.route('/choosedir/ajaxbackselected', methods=['POST'])
def choosedir_ajaxbackselected():
    ts = str(time.time())
    conn = sqlite3.connect("selected.sql")
    cur = conn.cursor()
    done_dir_split = request.form.get('done_dir', '').split("-->")
    cur.execute("INSERT INTO "+done_dir_split[1]+" VALUES ('"+done_dir_split[2]+"')")
    conn.commit()
    cur.close()
    conn.close()
    conn = sqlite3.connect("done.sql")
    cur = conn.cursor()
    print(done_dir_split[3])
    cur.execute("DELETE FROM dones WHERE dist='"+done_dir_split[3]+"'")
    conn.commit()
    cur.close()
    conn.close()
    if request.method == 'POST':
        return jsonify({'status':1})

@app.route('/choosedir/choosedownload', methods=['GET'])
def choosedir_choosedownload():
    content = ''
    conn = sqlite3.connect("done.sql")
    cur = conn.cursor()
    cur.execute("SELECT * FROM dones")
    for i in cur.fetchall():
        content += time.strftime("%b %d %H:%M:%S %Y", time.localtime(i[0]))+","+i[1]+","+i[2]+","+i[3]+'\r\n'
    cur.close()
    conn.close()       
    response = make_response(content)
    response.headers["Content-Disposition"] = "attachment; filename=log.csv"
    return response

@app.route('/choosedir/ajaxinsertdir', methods=['POST'])
def choosedir_ajaxinsertdir():
    conn = sqlite3.connect("unselect.sql")
    cur = conn.cursor()
    unselect_table = request.form.get('shareem', '') + "_" + request.form.get('titan', '')
    for i in request.form.get('dirs', '').split("\n"):
        if (i.rstrip()):
            cur.execute("INSERT INTO "+unselect_table+" VALUES ('"+i.rstrip()+"')")
    conn.commit()
    cur.close()
    conn.close()
    if request.method == 'POST':
        return jsonify({'status':1})



@app.route('/')
def index():
    return render_template('index')

@app.route('/camera')
def camera():
    return render_template('camera')

@app.route('/screen/<num>/<pos>')
def screen(num, pos):
    return render_template('screen', num=num, pos=pos)

def gen(camera, pos):
    while True:
        frame = camera.get_frame(pos)
        yield (b'--frame\r\nContent-Type: image/jpeg\r\n\r\n' + frame)

@app.route('/video_feed/<num>/<pos>')
def video_feed(num, pos):
    return Response(gen(VideoCamera(int(num)), pos), mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port="5002", debug=True, threaded=True)
    #pass

