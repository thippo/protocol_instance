#! /usr/bin/env python
# -*- coding: UTF-8 -*-
import requests
import sys
import json
import urllib3
import cv2
import re
import time
import ddddocr
import numpy as np

urllib3.disable_warnings()

ocr = ddddocr.DdddOcr()

class VX_obj():

    def __init__(self):
        self.Corpid = "xxxxxxxxxxxxxxxxx"
        self.Secret = "xxxxxxxxxx"
        self.Agentid = "xxxxxxxxxx"
        self.Token = 0

    def _GetTokenFromServer(self):
        Url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken"
        Data = {
            "corpid": self.Corpid,
            "corpsecret": self.Secret
        }
        r = requests.get(url=Url, params=Data, verify=False)
        #print(r.json())
        if r.json()['errcode'] != 0:
            return False
        else:

            self.Token = r.json()['access_token']

    def SendMessage(self, Partyid, Subject, Content):
        if self.Token==0:
            self._GetTokenFromServer()
        Url = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=%s" % self.Token
        Data = {
            "toparty": Partyid,
            "msgtype": "text",
            "agentid": self.Agentid,
            "text": {"content": Subject + '\n' + Content + "详见下图↓"},
            "safe": "0"
        }
        r = requests.post(url=Url, data=json.dumps(Data), verify=False)
        n = 1
        while r.json()['errcode'] != 0 and n < 3:
            n = n + 1
            self._GetTokenFromServer()
            if self.Token:
                Url = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=%s" % self.Token
                r = requests.post(url=Url, data=json.dumps(Data), verify=False)
                #print(r.json())
        return r.json()

    def get_media_id(self, path):
        if self.Token==0:
            self._GetTokenFromServer()
        media_url = "https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token={}&type=image".format(self.Token)
        with open(path, 'rb') as FR:
            files = {'media': FR}
            try:
                r = requests.post(media_url, files=files)
            except:
                self._GetTokenFromServer()
                r = requests.post(media_url, files=files)
        re = json.loads(r.text)
        return re['media_id']

    def SendImage(self, Partyid, pic_path):
        media_id = self.get_media_id(pic_path)
        url = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token={}".format(
            self.Token)
        data = {
            "toparty": Partyid,
            "msgtype": "image",
            "agentid": self.Agentid,
            "image":{
                "media_id": media_id
            },
            "safe": "0"
        }
        try:
            result = requests.post(url=url, data=json.dumps(data))
        except:
            self._GetTokenFromServer()
            result = requests.post(url=url, data=json.dumps(data))
        return result.text

def monitor_screen1():
    result = []
    video = cv2.VideoCapture(1)
    success, image = video.read()
    #image = cv2.imread("test.jpg")
    image_area = image[155:1050, 520:1100]
    img_hsv = cv2.cvtColor(image_area, cv2.COLOR_BGR2HSV)
    mask1 = cv2.inRange(img_hsv, (0,50,20), (5,255,255))
    mask2 = cv2.inRange(img_hsv, (175,50,20), (180,255,255))
    mask = cv2.bitwise_or(mask1, mask2)
    croped = cv2.bitwise_and(image_area, image_area, mask=mask)
    aa = np.where(mask>=1)
    image_vx = image[aa[0].min()+155-5:aa[0].max()+155+5, aa[1].min()+520-5:aa[1].min()+520+500]
    cv2.imwrite("shareEM2022.jpg", image_vx)
    result.append(["shareEM2022可能存在故障点\n", "shareEM2022.jpg"])
    return result

def monitor_screen2():
    result = []
    video = cv2.VideoCapture(2)
    success, image = video.read()

    image_h2 = image[142:170, 428:455]
    image_h2 = cv2.imencode(".jpg",image_h2,[cv2.IMWRITE_JPEG_QUALITY,90])
    res_h2 = re.sub("[^0-9]", "", ocr.classification(image_h2[1].tobytes()))
    if int(res_h2) >= 4:
        image_h2_all = image[131:181, 376:541]
        cv2.imwrite("H2.jpg", image_h2_all)
        result.append(["电池间H2浓度可能过高(>=4)\n", "H2.jpg"])

    image_temperature1 = image[121:136, 1400:1425]
    image_humidity1 = image[135:150, 1400:1425]
    image_temperature1 = cv2.imencode(".jpg",image_temperature1,[cv2.IMWRITE_JPEG_QUALITY,90])
    image_humidity1 = cv2.imencode(".jpg",image_humidity1,[cv2.IMWRITE_JPEG_QUALITY,90])
    res_temperature1 = re.sub("[^0-9]", "", ocr.classification(image_temperature1[1].tobytes()))
    res_humidity1 = re.sub("[^0-9]", "", ocr.classification(image_humidity1[1].tobytes()))

    image_temperature2 = image[218:233, 1397:1422]
    image_humidity2 = image[233:248, 1397:1422]
    image_temperature2 = cv2.imencode(".jpg",image_temperature2,[cv2.IMWRITE_JPEG_QUALITY,90])
    image_humidity2 = cv2.imencode(".jpg",image_humidity2,[cv2.IMWRITE_JPEG_QUALITY,90])
    res_temperature2 = re.sub("[^0-9]", "", ocr.classification(image_temperature2[1].tobytes()))
    res_humidity2 = re.sub("[^0-9]", "", ocr.classification(image_humidity2[1].tobytes()))

    lin_str = ""
    if (int(res_temperature1) >= 30 or int(res_temperature2) >= 30):
        lin_str += "配电室温度可能过高(>=30)\n"
    if (int(res_humidity1) >= 98 or int(res_humidity2) >= 98):
        lin_str += "配电室湿度可能过高(>=98)\n"
    if len(lin_str) > 1:
        image_distribution_room = image[116:253, 1245:1490]
        cv2.imwrite("image_distribution_room.jpg", image_distribution_room)
        result.append([lin_str, "image_distribution_room.jpg"])

    return result

if __name__ == '__main__':
    jspt_obj = VX_obj()
    Partyid_all = "21"
    Partyid_partial = "35"
    result_screen1 = monitor_screen1()
    for i in result_screen1:
        Status = jspt_obj.SendMessage(Partyid_all, "监控屏1", i[0])
        Status = jspt_obj.SendImage(Partyid_all, i[1])
        time.sleep(1)
    #result_screen2 = monitor_screen2()
    #for i in result_screen2:
    #    Status = jspt_obj.SendMessage(Partyid_all, "监控屏2", i[0])
    #    Status = jspt_obj.SendImage(Partyid_all, i[1])
    #    time.sleep(1)